# SSI-as-a-Service Project Summary

For more information, please contact: Sebastjan Pirih (sebastjan.pirih@netis.si) or Klemen Zajc (klemen.zajc@netis.si).

Project summary includes the following sections: 

[SUMMARY](https://gitlab.grnet.gr/essif-lab/business/SSIaaS/SSIaas_project_summary/-/blob/master/SSIaaS_project_summary.md#summary)
| [BUSINESS PROBLEM](https://gitlab.grnet.gr/essif-lab/business/SSIaaS/SSIaas_project_summary/-/blob/master/SSIaaS_project_summary.md#business-problem)
| [TECHNICAL (SOLUTION)](https://gitlab.grnet.gr/essif-lab/business/SSIaaS/SSIaas_project_summary/-/blob/master/SSIaaS_project_summary.md#technical-solution)
| [INTEGRATION](https://gitlab.grnet.gr/essif-lab/business/SSIaaS/SSIaas_project_summary/-/blob/master/SSIaaS_project_summary.md#integration)

## Introduction

### About us

[NETIS d.o.o.](https://www.netis.si/en/) is a Slovenian market leader and a pioneer in blockchain development, operating as a development company and a consultancy with a wide range of services evolving around blockchain technologies. The company has been growing rapidly since 2016 and currently employs 12 high profile experts in the field of blockchain. In the context of our own development, we have developed a proprietary blockchain framework, SSI-based modules and distributed storage solutions, operating under the [AceBlock](https://www.aceblock.com/) brand.

### Project SSI-as-a-Service (SSIaaS): Interoperable Authentication Layer to Connect the SSI Providers and Online Services

Although the field of SSI is gaining wider recognition, several challenges such as lack of standards, clear implementation documentation, security concerns and regulatory uncertainty inhibit its rapid adaptation. Consequently, the decision-making process for SSI authentication implementation can be time-consuming, and it is challenging to choose a trustworthy solution-provider that also meets all technological requirements. This will change with SSI-as-a-service, representing a one-stop-shop for developers by bringing a collection of services and libraries for SSI integration in one place. The solution, based on universality, compliance and interoperability, will therefore greatly simplify the SSI integration processes, and consequently accelerate its use in many services and industries, all leading to SSI mass adoption.

## Summary

### Business Problem

Due to the general lack of standards, clear implementation guidelines, security concerns and regulatory uncertainty, the SSI authentication implementation can be challenging for both businesses and developers.
To solve this problem, we suggest SSI-as-a-Service, serving as a one-point-shop that will offer developers a collection of services and libraries for SSI integration in one place. The solution will allow easy implementation and give assurance that components are interoperable, universal and compliant with all regulatory requirements.  This will simplify SSI integration and eliminate vendor-locking, whereas its universality will speed up SSI adoption into a wide range of services.
In order for all processes to run smoothly and efficiently, the platform will also provide technical support and customer service,
In designing the SSI-as-a-service platform we will strive to build trust, as we believe this is the crucial factor in building the reputability of SSI technology in general.

### Technical Solution

SSI-as-a-Service (SS) is a collection of services and libraries that enables a seamless SSI authentication integration into existing solutions. The main building blocks are:

>1. An SSI Server Wallet for RPs to manage authentication requests
>2. A DID registration service for registering/anchoring and resolving DIDs
>3. Authentication backend services for simple on-site integration of the SIOP DID Authentication for RPs (supporting implicit and authorization code flows)
>4. Authentication cloud service for a remote integration of the SIOP DID Authentication for RPs (supporting implicit and authorization code flows)
>5. A Trusted Entity Service, that issues a Trusted Entity Verifiable Credential after authentication with a sufficient LoA to the SS
>6. SSIaaS Website/Portal for customers (registration, packages, wallet download, documentation, integration examples, support)

You can find the technical details for all the components [here](https://gitlab.grnet.gr/essif-lab/business/SSIaaS/SSIaas_project_summary/-/blob/master/SSIaaS_project_summary.md#technical-solution).


## Integration with the eSSIF-Lab Functional Architecture and Community

Authentication service is responsible for all the authentication-related operations of a RP:
* creating authentication requests,
* validating authentication responses,
* issuing authorization and ID tokens,
* validating authorization tokens,
* validating DIDs.

All the aforementioned operations support two authentication protocols:
* OIDC ([https://openid.net/specs/openid-connect-core-1_0.html](https://openid.net/specs/openid-connect-core-1_0.html))
* DID SIOP ([https://identity.foundation/did-siop/](https://identity.foundation/did-siop/))


The authentication service is using SSI Server Wallet to perform cryptographic operations and it is using a Universal DID Resolver to resolve public DID Documents. The service supports two code flows:

* authorization code flow
* implicit flow (Self-Issued OID Provider)

The SSI Server Wallet, as presented in the previous sections, is responsible for creating and managing DIDs, signing, validating signatures, encryption and decryption.

The authentication service is delivered as a docker image that can run in different environments and exposes a set of REST APIs that support the aforementioned operations.

The service exposes the following collections:


| **Collection** | **Supported methods** | **Description** | **Notes** |
| ------ | ------ | ------ | ------ |
| /sessions | POST | Component authentication (mutual authentication) | - |
| /authentication-requests | POST | Create a SIOP DID authentication request | Protected |
| /authentication-responses | POST | Perform authentication response verification | Protected |
| /authorization-tokens | POST | Create an authorization token | Protected |
| /authorization-token-validations | POST | Validate an authorization token | May be public |
| /id-tokens | POST | Create an ID token | Protected |
| /id-token-validations | POST | Verify an ID token | May be public |

*Security considerations*

All APIs are secured and write communication is open only for authorized applications. Authorized applications need to perform a DID-OAuth2 authentication to obtain a component authorization token to consume the restricted APIs (applies to the SSI Server Wallet). The architecture follows the NIST zero trust architecture guidelines.

Configuration of the component is done via a configuration file.

Detailed information about integration with eSSIF-Lab Functional Architecture/Community and integration in general is provided [here](https://gitlab.grnet.gr/essif-lab/business/SSIaaS/SSIaas_project_summary/-/blob/master/SSIaaS_project_summary.md#integration).
