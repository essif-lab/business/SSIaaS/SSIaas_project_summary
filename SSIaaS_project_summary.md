# BOC-1 - Project Summary of SSI-as-a-Service (SSIaaS)

For more information, please contact: 

* Klemen Zajc (E: klemen.zajc@netis.si)
* Sebastjan Pirih (E: sebastjan.pirih@netis.si)

<!--
- name of the person to contact
-ways to contact this person (email, git, slack, others...)
--> 

<!--Try to be brief, to the point, and concrete in your descriptions. Appropriately refer/link to your own work and/or that of others.

Be mindful of the audience you target, which include other eSSIF-Lab subgrantees, project experts that evaluate your work, and other audiences in the NGI-EU community. Make sure your texts help them understand how your work may benefit theirs, what you need of them, and how everything fits together-->

## Summary
Although the field of SSI is gaining wider recognition, several challenges such as lack of standards, clear implementation documentation, security concerns and regulatory uncertainty inhibit its rapid adaptation. Consequently, the decision-making process for SSI authentication implementation can be time-consuming, and it is challenging to choose a trustworthy solution-provider that also meets all technological requirements. This will change with SSI-as-a-service, representing a one-stop-shop for developers by bringing a collection of services and libraries for SSI integration in one place. The solution, based on universality, compliance and interoperability, will therefore greatly simplify the SSI integration processes, and consequently accelerate its use in many services and industries, all leading to SSI mass adoption.
<!--Provide a *concise* description of your subproject, that helps other subgrantees to quickly determine what value there is if they work together with you, and/or let their results interact and interop with your results.-->

## Business Problem

Due to the general lack of standards, clear implementation guidelines, security concerns and regulatory uncertainty, the SSI authentication implementation can be challenging for both businesses and developers. 

To solve this problem, we suggest SSI-as-a-Service, serving as a one-point-shop that will offer developers a collection of services and libraries for SSI integration in one place. The solution will allow easy implementation and give assurance that components are interoperable, universal and compliant with all regulatory requirements.  This will simplify SSI integration and eliminate vendor-locking, whereas its universality will speed up SSI adoption into a wide range of services.

In order for all processes to run smoothly and efficiently, the platform will also provide technical support and customer service,

In designing the SSI-as-a-service platform we will strive to build trust, as we believe this is the crucial factor in building the reputability of SSI technology in general. 



<!--Provide a *concise* description of the business problem you focus on. What is the problem? Who needs to deal with it/suffers drawbacks?-->

## (Technical) Solution
SSI-as-a-Service (SS) is a collection of services and libraries that enables a seamless SSI authentication integration into existing solutions. The main building blocks are:

>1. An SSI Server Wallet for RPs to manage authentication requests
>2. A DID registration service for registering/anchoring and resolving DIDs
>3. Authentication backend services for simple on-site integration of the SIOP DID Authentication for RPs (supporting implicit and authorization code flows)
>4. Authentication cloud service for a remote integration of the SIOP DID Authentication for RPs (supporting implicit and authorization code flows)
>5. A Trusted Entity Service, that issues a Trusted Entity Verifiable Credential after authentication with a sufficient LoA to the SS
>6. SSIaaS Website/Portal for customers (registration, packages, wallet download, documentation, integration examples, support)



Below we provide technical details for all the components.

**1. SSI Server wallet**

SSI Server Wallet for RPs to manage authentication requests.

**2. DID Registration Service**

A DID registration service for registering/anchoring and resolving DIDs.

**3. Authentication service (on-site)**

Authentication service is responsible for all the authentication-related operations of a RP:
* creating authentication requests,
* validating authentication responses,
* issuing authorization and ID tokens,
* validating authorization tokens,
* validating DIDs.

All the aforementioned operations support two authentication protocols:
* OIDC ([https://openid.net/specs/openid-connect-core-1_0.html](https://openid.net/specs/openid-connect-core-1_0.html))
* DID SIOP ([https://identity.foundation/did-siop/](https://identity.foundation/did-siop/))


The authentication service is using SSI Server Wallet to perform cryptographic operations and it is using a Universal DID Resolver to resolve public DID Documents. The service supports two code flows:
* authorization code flow
* implicit flow (Self-Issued OID Provider)

The SSI Server Wallet, as presented in the previous sections, is responsible for creating and managing DIDs, signing, validating signatures, encryption and decryption.

The authentication service is delivered as a docker image that can run in different environments and exposes a set of REST APIs that support the aforementioned operations.

**APIs**

The service exposes the following collections:


| **Collection** | **Supported methods** | **Description** | **Notes** |
| ------ | ------ | ------ | ------ |
| /sessions | POST | Component authentication (mutual authentication) | - |
| /authentication-requests | POST | Create a SIOP DID authentication request | Protected |
| /authentication-responses | POST | Perform authentication response verification | Protected |
| /authorization-tokens | POST | Create an authorization token | Protected |
| /authorization-token-validations | POST | Validate an authorization token | May be public |
| /id-tokens | POST | Create an ID token | Protected |
| /id-token-validations | POST | Verify an ID token | May be public |

*Security considerations*

All APIs are secured and write communication is open only for authorized applications. Authorized applications need to perform a DID-OAuth2 authentication to obtain a component authorization token to consume the restricted APIs (applies to the SSI Server Wallet). The architecture follows the NIST zero trust architecture guidelines.

Configuration of the component is done via a configuration file.


**4. Authentication service (on-site)**

Cloud based authentication service is the authentication service, presented in the previous section, hosted in a cloud with an additional IAM component. An additional API security layer is required to prevent unauthorized access to the APIs.

The component needs to be connected with the SSI Server Wallet, that can run in the same or in a different environment. If both components run in the same environments, direct communication can be established. If the components run in different environments, we will be able to establish a secure connection between the two components via:

* Exposed SSI Server Wallet APIs with a secure component authentication 
* Establishing a secure stream between the two components (SSI Server Wallet connects with the Authentication Service) 
* Use P2P communication network (out of this scope) 

Security is of paramount importance, hence all the communication between the authentication component and the wallet is end-to-end encrypted and the component authentication is mutual.

Websites or services need to be whitelisted in Authentication service and use the /sessions API to obtain a component authorization token to consume APIs. The architecture design follows the NIST zero trust architecture guidelines.

**5. A Trusted Entity Service**

In the process of authentication, Verifiable credentials are exchanged, but that is not enough that relying party would trust that data are genuine. Verifier (can be RP itself, or perhaps trusted authentication service) must check that whoever issued VC can really be trusted. To simplify this process, we will setup a Trusted Entity Service, where at one place verifier can get the information about Trusted Entities (i.e. their publicly available data, including DID and DID document with their public keys).

Trusted Entity Service will:

* allow registration of new Trusted Entities
* issue a Trusted Entity Verifiable Credential after authentication with a sufficient LoA
* maintain a public list of Trusted Entities
* remove Trusted Entity from the list if it fails to maintain trust level

Although we will provide the technology solution, we will most likely not serve as an initial Trusted entity, but we plan to bootstrap entities that are already trusted from the people (government, municipalities, banks, post, universities etc.). They will then have the power to add other Trusted Entities if they can authenticate with a sufficient LoA.

Public Information about Trusted Entities will be stored on permissioned blockchain in smart contract that will allow modifications only from already trusted Entities. We will provide interfaces for easy management in browser and APIs for easy integration both on the management side and on the read-only side (for the purpose of verification).

**6. SSIaaS Portal**

When some website portal decides to implement SSI as an authentication method, the integration must be as seamless for them as possible. For that purpose we will offer SSIaaS Portal - one stop shop, where relying parties will get all components and information needed to enable SSI on their site. 

This portal will have the following features:
* registration of relying party
* SSI wallet download and instruction to setup
* packages (software development kits) for easy integration of SSI on websites in various programming languages
* documentation of all components with integration examples
* overview of SSI traffic and related costs
* support


Portal will be developed in one of the traditional frameworks (e.g. AngularJs). We will allow registration and login in traditional ways or with SSI. Documentation will be written in markdown, and rendered with one of the existing tools for that purpose (e.g. MkDocs).

Primarily we will offer our wallet and SDKs, however we intend to open it up also for other providers and thus allow faster development and preventing vendor lock in.



<!--Provide a *concise* list of the (concrete) results you will be providing (include a summary description of each of them), and how they (contribute to) solve the business problem(s)-->

## Integration

For integration, we identified two different area that we need to handle:
* integration with existing systems and 
* integration with other SSI components.

**Integration with existing systems**

Most of existing web portals are already using standard authentication approaches, and are familiar with their authentication flows and middleware that enables them. Our solution will try to stay to that approach as close as possible, meaning that those portals will have to upgrade just a tiny bit of their code.

Therefore we decided that in the first product that we will offer, we will prepare a package to use with NodeJs and a well known and widely used library Passport which enables easy authentication for NodeJs project. We will prepare a custom Passport Strategy, so everyone that is using Passport already will just need to add it to their code. The tokens it will receive in authentication flow will be compliant with tokens that it is receiving today, so none of the other code will need to be upgraded.

For projects that do not use Passport, we will prepare instructions for generic OIDC flows and an API based solution. Then we will gradually add easy integration solutions also for projects that use a different environment than NodeJs.

**Integration with other SSI components**

Authentication flow is closely connected to other SSI components:
* SSI server wallet
* SSI user wallet
* DID resolver
* Trusted Entity Service

These four components are involved in each authentication flow, so we will pay great attention to how they will be integrated. Although we will provide all of these components, we will not restrict the system to only our solution. The goal is to onboard also other providers to join the system and thus increase its usability and value. To allow the interoperability, we will propose some standard formats. If there appears need, we will set-up a service that will transform non standard data to defined format.

There is another crucial part in the authentication flow, related to issuance and verification of Verifiable Credentials. Here we impose no restrictions, since VC can be issued by any trusted Entity and ten simply attached to an ID token as one of the claims. The verification can be done in a way that was intended when VC were generated. This is important since it allows existing SSI solutions that already have components that issue and verify VC to be easily added to our authentication flow, without any changes in their existing procedures.

<!--Provide a precise description of how your results will fit with the eSSIF-Lab functional architecture (or modifications you need). Also, provide descriptions of how your results (are meant to) work, or link together in a meaningful way, both within the eSSIF-Lab family of projects, within EU-NGI, with US-DHS-SVIP, cross-border and/or other.-->

## <!--Your own section(s)-->

<!--Include sections for other stuff you want other eSSIF-Lab subgrantees (including those that applied for other calls) to become aware of.-->



